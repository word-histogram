module Main (main) where

import Data.Coerce (coerce)
import Data.Char (isSpace)
import Data.Ord (Down(Down))
import qualified Data.ByteString    as B        -- bytestring
import qualified Data.Text          as T        -- text
import qualified Data.Text.Encoding as T        -- text
import GHC.Conc (getNumCapabilities)
import System.Environment (getArgs)
import System.Exit (exitFailure)
import Text.Read (readMaybe)

import qualified Histogram as H
import TakeSort (takeSort, takeSortsPar, takeSortsParIO)
import Utf8Chunk (utf8Chunk)

data Parallel = Sequential | Strategies | Threads
  deriving (Read)

data Count = Chars | Words | Lines
  deriving (Read)

usage :: IO a
usage = do
  putStrLn "usage: word-histogram cores hpar tpar type count utf8.txt"
  putStrLn "hpar/tpar can be Sequential | Strategies | Threads"
  putStrLn "type can be Chars | Words | Lines"
  exitFailure

parseArgs :: IO (Int, Parallel, Parallel, Count, Int, FilePath)
parseArgs = do
  args <- getArgs
  case args of
    [coresS, histS, topS, countS, topCountS, fileName] ->
      case (,,,,,) <$>
           readMaybe coresS <*>
           readMaybe histS <*>
           readMaybe topS <*>
           readMaybe countS <*>
           readMaybe topCountS <*>
           Just fileName of
        Just s -> return s
        Nothing -> usage
    _ -> usage

main :: IO ()
main = do
  (cores0, histIO, topIO, count, topCount, fileName) <- parseArgs
  cores1 <- getNumCapabilities
  let cores
        | cores0 > 0 = cores0
        | otherwise = cores1
      log2cores = ceiling (logBase (2 :: Double) (fromIntegral cores))
  -- serial:   read the input file
  fileContents <- B.readFile fileName                 -- O(total bytes)
  -- serial:   split it into 1 chunk for each core
  let fileLength = B.length fileContents              -- O(1)
      chunkSize = div fileLength cores                -- O(1)
      strings = utf8Chunk canBreak chunkSize fileContents -- O(cores)
      canBreak = case count of
        Chars -> const True
        Words -> isSpace
        Lines -> ('\n' ==) -- FIXME check this works
      unpack = case count of
        Chars -> map T.singleton . T.unpack -- FIXME optimize, type system ouch
        Words -> T.words
        Lines -> T.lines
      -- O(total words / cores + unique words)
      histogram = case histIO of
        Sequential -> return . mconcat . map H.fromList
        Strategies -> return . H.fromListsPar
        Threads    -> H.fromListsParIO
      -- O(unique words * topCount / cores + cores * topCount)
      top = case topIO of
        Sequential -> \n -> return . takeSort n . concatMap (takeSort n)
        Strategies -> \n -> return . takeSortsPar n
        Threads    -> \n -> takeSortsParIO n
      source = map T.decodeUtf8 strings
  -- parallel: compute histogram for each chunk
  freq <- histogram $ map unpack source
  -- serial:   split the final histogram into disjoint submaps
  let freqs = map H.toFrequencyList $ H.splits log2cores freq
  -- parallel: extract the peaks
  result <- coerceOnF (map (Down . H.Frequency)) (top topCount) freqs
  putStr . unlines . map show $ result                -- O(topCount)

coerceOnF f g
    = fmap (coerce `asTypeOfReversed` f) . g . fmap (coerce `asTypeOf` f)
  where
    asTypeOfReversed :: (b -> a) -> (a -> b) -> (b -> a)
    asTypeOfReversed = const
