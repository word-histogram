module TakeSort
  ( takeSort
  , takeSortsPar
  , takeSortsParIO
  ) where

import Data.Coerce (coerce)
import Data.List (foldl', insert, transpose)
import Data.Maybe (catMaybes)
import Data.Ord (Down(Down))
import Control.Parallel.Strategies (parMap, rseq)       -- parallel
import Control.Concurrent.Spawn (parMapIO)              -- spawn
import Control.Exception (evaluate)

-- | Sort a list keeping only the first few values.
--   O(n * length xs)
takeSort
  :: Ord a
  => Int        -- ^ n
  -> [a]        -- ^ xs
  -> [a]        -- ^ take n (sort xs)
-- invariant: xs contains the (exactly) n largest values seen so far
-- head xs is the smallest value that makes the top n
-- if x is less than that, it will be pruned by the tail call
-- otherwise the new smallest value will be pruned
takeSort n
  = coercively (map getDown)
  . reverse
  . catMaybes
  . foldl' (\xs x -> tail (insert (Just x) xs)) (replicate n Nothing)
  . coercively (map Down)
{-# INLINE takeSort #-}

getDown :: Down a -> a
getDown (Down a) = a

coercively f = coerce `asTypeOf` f
{-# INLINE coercively #-}


takeSortsPar :: Ord a => Int -> [[a]] -> [a]
takeSortsPar n = takeSort n . concat . transpose . parMap rseq (takeSort n)
{-# INLINE takeSortsPar #-}

takeSortsParIO :: Ord a => Int -> [[a]] -> IO [a]
takeSortsParIO n = fmap (takeSort n . concat . transpose) . parMapIO (evaluate . takeSort n)
{-# INLINE takeSortsParIO #-}
