word-histogram
==============

Count frequencies of chars/words/lines in UTF-8 text files using three kinds of
parallelism (sequential, Control.Parallel.Strategies, Control.Concurrent.Spawn).

Inspired by: <https://github.com/apauley/parallel-frequency>


Usage
-----

    word-histogram cores hpar tpar type count utf8

    cores  -- the amount of parallelism to use
              (0 uses getNumCapabilities)
    
    hpar   -- the parallel method for histogram creation
              (Sequential | Strategies | Threads)
    
    tpar   -- the parallel method for "top N" extraction
              (Sequential | Strategies | Threads)
    
    type   -- what to histogram on
              (Chars | Words | Lines)
    
    count  -- how many items to extract
              (the N in "top N")
    
    utf8   -- the input filename
              (must contain valid UTF-8 encoded text)

    All arguments are mandatory and must occur in order.


Benchmarks
----------

AMD Athlon(tm) II X4 640 Processor (quad core, 3GHz, 8GB RAM)
ghc-7.10.1 (Linux amd64 bindist)


    $ ls -1sh artamene.txt
    11M artamene.txt
    $ word-histogram +RTS -N -s -A64M -RTS 4 Strategies Sequential Words 10 artamene.txt
    ("de",87178)
    ("que",67057)
    ("et",47254)
    (":",44569)
    ("la",42343)
    ("\224",34063)
    ("ne",32839)
    ("vous",29862)
    ("le",29532)
    ("je",29121)
       1,489,660,416 bytes allocated in the heap
          34,407,248 bytes copied during GC
          27,761,352 bytes maximum residency (1 sample(s))
           2,389,512 bytes maximum slop
                 305 MB total memory in use (0 MB lost due to fragmentation)
    
                                         Tot time (elapsed)  Avg pause  Max pause
      Gen  0         6 colls,     5 par    0.132s   0.042s     0.0071s    0.0155s
      Gen  1         1 colls,     1 par    0.020s   0.006s     0.0057s    0.0057s
    
      Parallel GC work balance: 69.65% (serial 0%, perfect 100%)
    
      TASKS: 10 (1 bound, 9 peak workers (9 total), using -N4)
    
      SPARKS: 4 (4 converted, 0 overflowed, 0 dud, 0 GC'd, 0 fizzled)
    
      INIT    time    0.000s  (  0.004s elapsed)
      MUT     time    2.524s  (  0.783s elapsed)
      GC      time    0.152s  (  0.048s elapsed)
      EXIT    time    0.004s  (  0.004s elapsed)
      Total   time    2.680s  (  0.839s elapsed)
    
      Alloc rate    590,198,263 bytes per MUT second
    
      Productivity  94.3% of total user, 301.4% of total elapsed
    
    gc_alloc_block_sync: 8943
    whitehole_spin: 0
    gen[0].sync: 2081
    gen[1].sync: 12


    $ ls -1sh books.txt
    29M books.txt
    $ word-histogram +RTS -N -s -A64M -RTS 4 Strategies Sequential Words 10 books.txt
    ("the",158933)
    ("and",107854)
    ("of",89036)
    ("de",87430)
    ("que",67091)
    ("to",66370)
    ("a",50123)
    ("et",47304)
    (":",44569)
    ("in",43127)
       4,246,763,552 bytes allocated in the heap
         125,486,528 bytes copied during GC
          63,881,184 bytes maximum residency (2 sample(s))
           4,052,400 bytes maximum slop
                 365 MB total memory in use (0 MB lost due to fragmentation)
    
                                         Tot time (elapsed)  Avg pause  Max pause
      Gen  0        18 colls,    18 par    0.512s   0.174s     0.0097s    0.0164s
      Gen  1         2 colls,     1 par    0.024s   0.008s     0.0041s    0.0067s
    
      Parallel GC work balance: 58.09% (serial 0%, perfect 100%)
    
      TASKS: 10 (1 bound, 9 peak workers (9 total), using -N4)
    
      SPARKS: 4 (3 converted, 0 overflowed, 0 dud, 0 GC'd, 1 fizzled)
    
      INIT    time    0.000s  (  0.004s elapsed)
      MUT     time    8.080s  (  2.522s elapsed)
      GC      time    0.536s  (  0.183s elapsed)
      EXIT    time    0.004s  (  0.002s elapsed)
      Total   time    8.620s  (  2.710s elapsed)
    
      Alloc rate    525,589,548 bytes per MUT second
    
      Productivity  93.8% of total user, 298.3% of total elapsed
    
    gc_alloc_block_sync: 109141
    whitehole_spin: 0
    gen[0].sync: 1796
    gen[1].sync: 0


    $ ls -1sh gutenberg.rdf
    673M gutenberg.rdf
    $ word-histogram +RTS -N -s -A64M -RTS 4 Strategies Sequential Words 10 gutenberg.rdf
    ("</rdf:Description>",1051760)
    ("<rdf:Description",1051752)
    ("<dcam:memberOf",966656)
    ("<rdf:value",830516)
    ("rdf:resource=\"http://purl.org/dc/terms/IMT\"/>",781337)
    ("<dcterms:format>",781337)
    ("</dcterms:format>",781337)
    ("<pgterms:file",661624)
    ("<dcterms:modified",661624)
    ("<dcterms:isFormatOf",661624)
      22,842,886,616 bytes allocated in the heap
       1,746,765,656 bytes copied during GC
       1,417,475,456 bytes maximum residency (3 sample(s))
          19,399,528 bytes maximum slop
                2388 MB total memory in use (0 MB lost due to fragmentation)
    
                                         Tot time (elapsed)  Avg pause  Max pause
      Gen  0        95 colls,    95 par    7.340s   2.716s     0.0286s    0.2101s
      Gen  1         3 colls,     2 par    0.056s   0.098s     0.0328s    0.0894s
    
      Parallel GC work balance: 43.40% (serial 0%, perfect 100%)
    
      TASKS: 10 (1 bound, 9 peak workers (9 total), using -N4)
    
      SPARKS: 4 (4 converted, 0 overflowed, 0 dud, 0 GC'd, 0 fizzled)
    
      INIT    time    0.004s  (  0.004s elapsed)
      MUT     time   75.564s  ( 24.771s elapsed)
      GC      time    7.396s  (  2.814s elapsed)
      EXIT    time    0.028s  (  0.090s elapsed)
      Total   time   82.992s  ( 27.678s elapsed)
    
      Alloc rate    302,298,536 bytes per MUT second
    
      Productivity  91.1% of total user, 273.1% of total elapsed
    
    gc_alloc_block_sync: 260218
    whitehole_spin: 0
    gen[0].sync: 21626
    gen[1].sync: 0


Notes
-----

The main low-level hack that achieves impressive parallel speed-up is chunking
the input UTF-8 text file into an appropriate number of pieces using O(1)
ByteString operations before even starting any histogram generation.

Map.fromListWith (+) is about as fast as any sequential method to apply to
each chunk (possibly in parallel), which are then merged sequentially with
Map.unionsWith (+).

Extracting the "top N" values is parallelized by splitting the histogram into
disjoint pieces and finding the top N of each piece in parallel.  The "top N"
of the whole histogram is then the "top N" of concatenation of the piece
"top N"s.  But this operation doesn't seem to get as much parallel speedup.


-- 
http://code.mathr.co.uk/word-histogram
