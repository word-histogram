{-# LANGUAGE RoleAnnotations #-}
module Histogram
  ( Histogram()
  , fromFrequencyList
  , toFrequencyList
  , fromList
  , fromMap
  , toMap
  , split
  , splits
  , Frequency(..)
  , fromListsPar
  , fromListsParIO
  , fromFrequencyListsPar
  , fromFrequencyListsParIO
  )
  where

import Control.Parallel.Strategies (parMap, rseq)       -- parallel
import Control.Concurrent.Spawn (parMapIO)              -- spawn
import Control.Exception (evaluate)
import qualified Data.Map.Strict as M                   -- containers
import Data.Monoid (Monoid(..))

-- | A histogram.
type role Histogram nominal
newtype Histogram a = Histogram{ getHistogram :: M.Map a Int }
  deriving (Read, Show, Eq, Ord)

newtype Frequency a = Frequency{ getFrequency :: (a, Int) }
  deriving (Read, Show, Eq)

instance Ord a => Ord (Frequency a) where
  compare (Frequency (x, a)) (Frequency (y, b)) = compare a b `mappend` compare x y

instance Ord a => Monoid (Histogram a) where
  mempty = Histogram M.empty
  mconcat = Histogram . M.unionsWith (+) . map getHistogram

instance Ord a => Semigroup (Histogram a) where
  Histogram a <> Histogram b = Histogram (M.unionWith (+) a b)

fromFrequencyList
  :: Ord a
  => [(a, Int)]
  -> Histogram a
fromFrequencyList = Histogram . M.fromListWith (+)
{-# INLINE fromFrequencyList #-}

toFrequencyList
  :: Histogram a
  -> [(a, Int)]
toFrequencyList = M.toList . getHistogram
{-# INLINE toFrequencyList #-}

fromList
  :: Ord a
  => [a]          -- ^ values
  -> Histogram a  -- ^ histogram
fromList = fromFrequencyList . map (\w -> (w, 1))
{-# INLINE fromList #-}

fromMap
  :: M.Map a Int
  -> Histogram a
fromMap = Histogram
{-# INLINE fromMap #-}

toMap
  :: Histogram a
  -> M.Map a Int
toMap = getHistogram
{-# INLINE toMap #-}


-- | Split a histogram into disjoint pieces.  How many pieces and their sizes
--   depends on the implementation of 'Data.Map.splitRoot', which is iterated
--   'depth' times.
splits
  :: Int            -- ^ depth must be non-negative
  -> Histogram a    -- ^ histogram
  -> [Histogram a]  -- ^ histogram pieces
splits depth
  = map Histogram
  . (!! depth) . iterate (concatMap M.splitRoot) . (:[])
  . getHistogram
{-# INLINE splits #-}

-- | Split a histogram once.
--
--   > split == splits 1
split
  :: Histogram a    -- ^ histogram
  -> [Histogram a]  -- ^ histogram pieces
split = splits 1
{-# INLINE split #-}

fromListsPar :: Ord a => [[a]] -> Histogram a
fromListsPar = mconcat . parMap rseq fromList
{-# INLINE fromListsPar #-}

fromFrequencyListsPar :: Ord a => [[(a, Int)]] -> Histogram a
fromFrequencyListsPar = mconcat . parMap rseq fromFrequencyList
{-# INLINE fromFrequencyListsPar #-}

fromListsParIO :: Ord a => [[a]] -> IO (Histogram a)
fromListsParIO = fmap mconcat . parMapIO (evaluate . fromList)
{-# INLINE fromListsParIO #-}

fromFrequencyListsParIO :: Ord a => [[(a, Int)]] -> IO (Histogram a)
fromFrequencyListsParIO = fmap mconcat . parMapIO (evaluate . fromFrequencyList)
{-# INLINE fromFrequencyListsParIO #-}
